import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

/**
 * University of San Diego
 * COMP 285: Spring 2015
 * Instructor: Gautam Wilkins
 *
 * Implement your Connect Four player class in this file.
 */
public class MyPlayer extends Player {

    Random rand;

    private class Move {
        int move;
        double value;

        Move(int move) {
            this.move = move;
            this.value = 0.0;
        }
    }

    public MyPlayer() {
        rand = new Random();
        return;
    }

    public void setPlayerNumber(int number) {
        this.playerNumber = number;
    }

    public int chooseMove(Board gameBoard) {

        long start = System.nanoTime();

        Move bestMove = search(gameBoard, 6, this.playerNumber);
        System.out.println(bestMove.value);

        long diff = System.nanoTime()-start;
        double elapsed = (double)diff/1e9;
        System.out.println("Elapsed Time: " + elapsed + " sec, Player: " + playerNumber);
        return bestMove.move;
    }

    public Move search(Board gameBoard, int maxDepth, int playerNumber) {

        ArrayList<Move> moves = new ArrayList<Move>();

        for (int i=0; i<Board.BOARD_SIZE; i++) {
            if (!gameBoard.isColumnOpen(i)) {
                continue;
            }

            Move thisMove = new Move(i);
            gameBoard.move(playerNumber, i);

            int gameStatus = gameBoard.checkIfGameOver(i);
            if (gameStatus >= 0) {
                if (gameStatus == 0) {
                    thisMove.value = 0.0;
                } else if (gameStatus == playerNumber) {
                    thisMove.value = 1.0;
                } else {
                    thisMove.value = -1.0;
                }

            } else if (maxDepth == 0) {
                thisMove.value = heuristic(gameBoard, playerNumber);

            } else {
                Move responseMove = search(gameBoard, maxDepth-1, (playerNumber == 1 ? 2 : 1));
                thisMove.value = -responseMove.value;
            }
            moves.add(thisMove);
            gameBoard.undoMove(i);
        }
        return this.getBestMove(moves);

    }

    public double heuristic(Board gameBoard, int playerNumber) {
        double score = 0;
        score += checkSpecialRowCase(gameBoard, playerNumber);
        score += checkDownDiagonal(gameBoard, playerNumber);
        score += checkUpDiagonal(gameBoard, playerNumber);
        score += checkReverseDiagonal(gameBoard, playerNumber);
        score += checkUpReverseDiagonal(gameBoard, playerNumber);
        score += checkColumn(gameBoard, playerNumber);
        score += checkRow(gameBoard, playerNumber);
        score += checkDiagonal(gameBoard, playerNumber);

        if(score == 0){
            return 0;
        }
        double finalScore = 1 - (1/score);
        return finalScore;
    }

    private int getOp(int player){
        return (player == 1) ? 2 : 1;
    }


    private int checkSpecialRowCase(Board board, int player){
        int inARow = 0, gap = 0, gapColumn = 0, gapCheck = 0;
        int[][] newBoard = board.getBoard();
        for(int i = 0; i < 7; i++){
            for(int j = 0; j < 7; j++){
                if(newBoard[i][j] == 0){
                    if(inARow != 0){
                        gapColumn = j;
                        gap++;
                    }
                }else if(newBoard[i][j] == player){
                    if(gap > 1) {
                        gap = 0;
                        inARow = 0;
                    }
                    inARow++;
                    if(gap == 1 && inARow == 3)
                        for(int c = 0; c < 7; c++)
                            if(newBoard[gapColumn][c] != 0) {
                                gapCheck++;
                                return 1;
                            }
                }if(j == 6){
                    inARow = 0;
                }
            }
        }
        return 0;
    }

    private int checkRow(Board board, int player){
        int inARow = 0;
        int[][] newBoard = board.getBoard();
        for(int i = 0; i < 7; i++){
            for(int j = 0; j < 7; j++){
                if(newBoard[i][j] == 0){
                    inARow = 0;
                }else if(newBoard[i][j] == player){
                    inARow++;
                }else{
                    inARow = 0;
                }
            }
            if(inARow == 3){
                return 1;
            }
            if(i == 6)
                inARow = 0;
        }
        return 0;
    }

    private int checkColumn(Board board, int player){
        int inARow = 0;
        int[][] newBoard = board.getBoard();
        for(int i = 0; i < 7; i++){
            for(int j = 0; j < 7; j++){
                if(newBoard[j][i] == 0){
                    inARow = 0;
                }else if(newBoard[j][i] == player){
                    inARow++;
                }else{
                    inARow = 0;
                }
            }
            if(inARow == 3){
                return 1;
            }
        }
        return 0;
    }

    private int checkUpDiagonal(Board board, int player){
        int[][] newBoard = board.getBoard();
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++){
                if (newBoard[i][j] == player) {
                    if (newBoard[i + 1][j + 1] == player) {
                        if (newBoard[i + 3][j + 2] == player) {
                            if(newBoard[i + 2][j + 1] == 0)
                                continue;
                            else
                                return 1;
                        }
                    }

                }
            }
        }
        return 0;
    }

    private int checkDownDiagonal(Board board, int player){
        int[][] newBoard = board.getBoard();
        for (int i = 0; i < 4; i++) {
            for (int j = 6; j > 3; j--){
                if (newBoard[i][j] == player) {
                    if (newBoard[i + 1][j - 1] == player) {
                        if (newBoard[i + 3][j - 2] == player) {
                            if(newBoard[i + 2][j - 1] == 0)
                                continue;
                            else
                                return 1;
                        }
                    }

                }
            }
        }
        return 0;
    }

    private int checkUpReverseDiagonal(Board board, int player){
        int[][] newBoard = board.getBoard();
        int a, b, c;
        for (int i = 0; i < 4; i++) {
            for (int j = 6; j < 2; j--){
                if (newBoard[i][j] == player) {
                    if (newBoard[i + 1][j - 1] == player) {
                        if (newBoard[i + 3][j - 2] == player) {
                            if(newBoard[i + 1][j - 2] == 0)
                                continue;
                            else if(newBoard[i + 2][j - 2] != 0)
                                continue;
                            else
                                return 1;
                        }
                    }

                }
            }
        }
        return 0;
    }

    private int checkReverseDiagonal(Board board, int player){
        int[][] newBoard = board.getBoard();
        for (int i = 6; i > 2; i--) {
            for (int j = 6; j > 3; j--){
                if (newBoard[i][j] == player) {
                    if (newBoard[i - 1][j - 1] == player) {
                        if (newBoard[i - 3][j - 3] == player) {
                            if(newBoard[i - 2][j - 3] == 0)
                                continue;
                            else if(newBoard[i - 2][j - 2] != 0)
                                continue;
                            else
                                return 1;
                        }
                    }

                }
            }
        }
        return 0;
    }


    private int checkDiagonal(Board board, int player) {
        int connectNum = 0;
        int[][] gameBoard =  board.getBoard();


        for (int i=0, j=0; i < 7 && j<7; i++, j++) {
            if (gameBoard[i][j] == player) {
                connectNum++;
            } else {
                break;
            }
        }

        for (int i=0-1, j=0-1; i>=0 && j >= 0; i--, j--) {
            if (gameBoard[i][j] == player) {
                connectNum++;
            } else {
                break;
            }
        }

        if (connectNum >= 4) {
            return 1;
        }
        connectNum = 0;

        for (int i=0, j=0; i < 7 && j>=0; i++, j--) {
            if (gameBoard[i][j] == player) {
                connectNum++;
            } else {
                break;
            }
        }

        for (int i=0-1, j=0+1; i >= 0 && j < 7; i--, j++) {
            if (gameBoard[i][j] == player) {
                connectNum++;
            } else {
                break;
            }
        }

        if (connectNum >= 4) {
            return 1;
        }

        return 0;
    }
    

    private Move getBestMove(ArrayList<Move> moves) {

        double max = -1.0;
        Move bestMove = moves.get(0);

        for (Move cm : moves) {
            if(cm.value == 0.0){
                continue;
            }else if (cm.value > max) {
                max = cm.value;
                bestMove = cm;
            }
        }
        return bestMove;
    }
}